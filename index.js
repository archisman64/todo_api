const dotenv = require('dotenv');
dotenv.config();
const setupDb = require('./src/config/database');
const express = require('express');
const app = express();

setupDb();

app.use(express.json());

// const User = require('./models/User');
const userRoutes = require('./src/routes/userRoutes');
const projectRoutes = require('./src/routes/projectRoutes');
const taskRoutes = require('./src/routes/taskRoutes');

app.use('/api/user', userRoutes);
app.use('/api/project', projectRoutes);
app.use('/api/task', taskRoutes);
app.use('/*', (req, res) => {
    res.status(404).json({ success: false, message: 'Page not found.' });
});

const port = process.env.PORT || 3000;

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});