# TODO API Documentation

This API provides endpoints for managing user projects and tasks.

## Endpoints

### User

- `POST /api/user`: Create a new user.
- `GET /api/user/:username`: Get user profile details.

Response: 
```
{
    "success": true,
    "message": "Profile found.",
    "data": {
        "id": 5,
        "username": "updated username",
        "email": "sampleuser@test.com",
        "password": "$2b$10$vk/4VlXS0fL2pe51ZuB5f.KIktI9DriJxxGAL8OMPc9afr3hym0K2",
        "name": "updated name",
        "created_at": "2023-08-19T07:34:04.843Z",
        "updated_at": "2023-08-19T07:34:04.843Z"
    }
}
```

- `POST /api/user/signup`: Sign up as a new user.

Request body:
```
{
    "username": "sample_user",
    "email": "sampleuser@test.com",
    "password": "12345678",
    "name": "sampleuser"
}
```
Response:
```
{
    "success": true,
    "message": "User created successfully.",
    "data": {
        "username": "sample_user",
        "email": "sampleuser@test.com",
        "password": "$2b$10$vk/4VlXS0fL2pe51ZuB5f.KIktI9DriJxxGAL8OMPc9afr3hym0K2",
        "name": "sampleuser",
        "id": 5
    }
}
```
- `POST /api/user/login` : Login with email and password in request body to get a token.

Request body:
```
{
    "username": "sample_user",
    "password": "12345678"
}
```
Response:
```
{
    "success": true,
    "data": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InNhbXBsZXVzZXJAdGVzdC5jb20iLCJpYXQiOjE2OTI0MzU4MTIsImV4cCI6MTY5MjQzOTQxMn0.Vjbig2mFXAJvtI5ASSo93IcS9uHmGmM2wASZmdVLUAo"
}
```
- `PUT /api/user/:username`: Update user profile.

Reuqest body:
```
{
    "name": "updated name",
    "username": "updated username"
}
```
Response:
```
{
    "success": true,
    "message": "Profile updated successfully",
    "data": 1
}
```

### Project

- `POST /api/project`: Create a new project.

Reuqest body:
```
{
    "title": "First project",
    "description": "Description for First Project",
    "tasks": [
        {
            "title": "First task",
            "description": "Description for First Task of First Project"
        },
        {
            "title": "Second task",
            "description": "Description for Second Task of Second Project"
        }
    ],
    "user_id": 5
}
```
Response:
```
{
    "success": true,
    "message": "New project created.",
    "data": {
        "title": "First project",
        "description": "Description for First Project",
        "tasks": [
            {
                "title": "First task",
                "description": "Description for First Task of First Project",
                "project_id": 5,
                "done": false,
                "id": 6
            },
            {
                "title": "Second task",
                "description": "Description for Second Task of Second Project",
                "project_id": 5,
                "done": false,
                "id": 7
            }
        ],
        "user_id": 5,
        "done": false,
        "id": 5
    }
}
```
- `GET /api/project/:id`: Get project details.

Response:
```
{
    "success": true,
    "message": "Project found.",
    "data": {
        "id": 5,
        "title": "First project",
        "description": "Description for First Project",
        "done": false,
        "user_id": 5,
        "created_at": "2023-08-19T09:26:44.608Z",
        "updated_at": "2023-08-19T09:26:44.608Z",
        "tasks": [
            {
                "id": 6,
                "title": "First task",
                "description": "Description for First Task of First Project",
                "done": false,
                "project_id": 5,
                "created_at": "2023-08-19T09:26:44.622Z",
                "updated_at": "2023-08-19T09:26:44.622Z"
            },
            {
                "id": 7,
                "title": "Second task",
                "description": "Description for Second Task of Second Project",
                "done": false,
                "project_id": 5,
                "created_at": "2023-08-19T09:26:44.622Z",
                "updated_at": "2023-08-19T09:26:44.622Z"
            }
        ]
    }
}
```
- `PUT /api/project/:id`: Update project details.

Request body:
```
{
    "title": "Updated First Project",
    "description": "Updated description for First Project.",
    "tasks": [
        {
            "title": "First task",
            "description": "Description for First Task of First Project",
            "done": false
        },
        {
            "title": "Second task",
            "description": "Description for Second Task of Second Project",
            "done": false
        }
    ]
}
```
Response:
```
{
    "sucess": true,
    "message": "Project updated successfully.",
    "data": {
        "id": 5,
        "title": "Updated First Project",
        "description": "Updated description for First Project.",
        "done": false,
        "user_id": 5,
        "created_at": "2023-08-19T09:26:44.608Z",
        "updated_at": "2023-08-19T09:26:44.608Z",
        "tasks": [
            {
                "id": 12,
                "title": "First task",
                "description": "Description for First Task of First Project",
                "done": false,
                "project_id": 5,
                "created_at": "2023-08-19T10:49:14.884Z",
                "updated_at": "2023-08-19T10:49:14.884Z"
            },
            {
                "id": 13,
                "title": "Second task",
                "description": "Description for Second Task of Second Project",
                "done": false,
                "project_id": 5,
                "created_at": "2023-08-19T10:49:14.884Z",
                "updated_at": "2023-08-19T10:49:14.884Z"
            }
        ]
    }
}
```
- `DELETE /api/project/:id`: Delete a project.

Response:
```
{
    "success": true,
    "message": "Project deleted successfully"
}
```

### Task

- `POST /api/task`: Create a new task for a project.

Request body:
```
{
  "title": "New Task for First Project",
  "description": "Some Description",  // Optionally specify the "done" status
  "project_id": 5  // ID of the project to which this task belongs
}
```
Response: 
```
{
    "success": true,
    "message": "Successfully added new task.",
    "data": {
        "title": "New Task for First Project",
        "description": "Some Description",
        "project_id": 5,
        "done": false,
        "id": 6
    }
}
```
- `PUT /api/task/:id`: Update task details.

Request body:
```
{
    "done": true,
    "project_id": 5
}
```
Response:
```
{
    "success": true,
    "message": "Successfully updated task.",
    "data": {
        "done": true,
        "project_id": 5,
        "id": 6,
        "title": "New Task for First Project",
        "description": "Some Description",
        "created_at": "2023-08-18T11:29:18.618Z",
        "updated_at": "2023-08-18T11:29:18.618Z"
    }
}
```
- `DELETE /api/task/:id`: Delete a task.

Response:
```
{
    "success": true,
    "message": "Task deleted successfully"
}
```

## Authentication

- User registration and login are required to access the protected routes (projects, tasks and      profile update).
- Upon successful login, a JWT token is provided in the response.
- Include the token in the `Authorization` header as a Bearer token for protected routes.

## Project Status Updates

- When all tasks of a project are marked as done, the project's `done` status is automatically updated to `true`.
- When a new task is added to a project, the project's `done` status is automatically updated to `false`.

## Error Responses

- The API returns appropriate error responses with status codes and error messages for various scenarios.
- Verify the error responses to handle potential issues during API requests.

## How to Use

1. Clone this repository.
2. Install dependencies: `npm install`.
3. Configure your PostgreSQL database connection in `knexfile.js`.
4. Run migrations: `npm run migrate`.
5. Start the server: `npm start`.

## Environment Variables

- `DB_URL`: PostgreSQL database URL for production environment.
- `JWT_SECRET`: Secret key for JWT token generation.
- `PORT`: Port for the server to listen on.

## Contributing

Feel free to contribute to this project by submitting pull requests or reporting issues.
