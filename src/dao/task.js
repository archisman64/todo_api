const Task = require('../models/Task');
const Project = require('../models/Project');

const findTaskById = (id) => {
    return Task.query().findById(id);
}

const createTask = async (newTask) => {
    const result = await Task.query().insert(newTask);
    await Project.query().patch({ done: false }).where({ id: result.project_id });
    return result;
}

const updateTask = async (taskId, modifiedTask) => {
    const result = await Task.query().patchAndFetchById(taskId, modifiedTask);
    // Get the project_id associated with the task
    const projectId = result.project_id;
    // Check if all tasks for the project are done
    const allTasksDone = await Task.query().where({ project_id: projectId, done: false }).count().first();
    // console.log('count of remaining tasks:', allTasksDone.count);
    await Project.query().patch({ done: allTasksDone.count == 0 ? true : false }).where({ id: projectId });
    return result;
}

const deleteTask = (taskId) => {
    return Task.query().deleteById(taskId);
}

module.exports = {
    createTask,
    updateTask,
    deleteTask,
    findTaskById
}