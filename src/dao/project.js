const Project = require('../models/Project');

const insertProject = (projectInfo) => {
    return Project.query().insertGraph(projectInfo);
}

const findProject = (projectId) => {
    return Project.query().findById(projectId).withGraphFetched('tasks');
}

const findAllProjects = (userId) => {
    return Project.query().where('user_id', userId).withGraphFetched('tasks');
}

const updateProject = (projectId, newInfo) => {
    return Project.query().upsertGraphAndFetch({
        id: projectId,
        ...newInfo
    });
}

const removeProject = (projectId) => {
    return Project.query().deleteById(projectId);
}

module.exports = {
    insertProject,
    findProject,
    updateProject,
    removeProject,
    findAllProjects
}