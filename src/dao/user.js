const User = require('../models/User');

const findByUsername = (userName) => {
    return User.query().where("username", userName).first();
}

const findByEmail = (email) => {
    return User.query().findOne({ email });
}

const insertUser = (userInfo) => {
    return User.query().insert(userInfo);
}

const updateUser = (userId, updatedUserInfo) => {
    return User.query().findById(userId).patch(updatedUserInfo);
}

module.exports = {
    findByUsername,
    findByEmail,
    insertUser,
    updateUser
};