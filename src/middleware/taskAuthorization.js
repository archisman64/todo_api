const taskService = require('../services/taskServices');
const proejctService = require('../services/proejctServices');

async function verifyTaskBelongsToUser(req, res, next) {
    const { user } = req;
    const taskId = req.params.id;

    try {
        const task = await taskService.findTaskById(taskId);
        if (!task) {
            return res.status(404).json({ success: false, message: 'Task not found' });
        }
        const projectId = task.project_id;
        const project = await proejctService.getProject(projectId);
        if (!project) {
            return res.status(404).json({ success: false, message: 'Associated project not found' });
        }
        if (project.user_id !== user.id) {
            return res.status(403).json({ success: false, message: 'Access denied: Task does not belong to the user' });
        }
        // The task belongs to the user, continue to the next middleware/controller
        next();
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'An error occurred while verifying task ownership' });
    }
}

module.exports = verifyTaskBelongsToUser;

