const jwt = require('jsonwebtoken');
const userService = require('../services/userServices');

const verifyToken = (request, response, next) => {
    const token = request.headers.authorization;
    // console.log('token:', token);
    if (!token) {
        return response.status(401).json({ success: false, message: 'Unauthorized' });
    }

    jwt.verify(token, process.env.JWT_SECRET, async (error, data) => {
        if (error) {
            return response.status(403).json({ success: false, message: error.message });
        }

        const email = data.email;
        request.user = await userService.getUserByEmail(email);
        next();
    });
}

module.exports = verifyToken;
