const { Model } = require('objection');
const Knex = require('knex');
const knexConfig = require('../../knexfile');

const setupDb = async () => {
    const knex = Knex(knexConfig.development);

    knex.on('error', (error) => {
        console.error('Database connection error:', error);
    });

    Model.knex(knex);

    try {
        // Test the connection by executing a query
        await knex.raw('SELECT 1');
        console.log('Database connection successful');
    } catch (error) {
        console.error('Error connecting to the database:', error);
    }
}

module.exports = setupDb;