const express = require('express');
const router = express.Router();
const verifyToken = require('../middleware/auth');

const userController = require('../controllers/userController');

router.post('/', userController.createUser);
router.get('/:username', userController.getUserProfile);
router.put('/:username', verifyToken, userController.updateProfile);
router.post('/signup', userController.signUpUser);
router.post('/login', userController.loginUser);

module.exports = router;