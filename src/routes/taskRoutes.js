const express = require('express');
const router = express.Router();
const verifyToken = require('../middleware/auth');
const taskAuthorization = require('../middleware/taskAuthorization');

const taskController = require('../controllers/taskController');

router.post('/', verifyToken, taskController.createTask);
router.put('/:id', verifyToken, taskAuthorization, taskController.updateTaskData);
router.delete('/:id', verifyToken, taskAuthorization, taskController.deleteTask);

module.exports = router;