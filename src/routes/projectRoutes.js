const express = require('express');
const router = express.Router();
const verifyToken = require('../middleware/auth');

const projectController = require('../controllers/projectController');

router.post('/', verifyToken, projectController.createProject);
router.get('/:id', verifyToken, projectController.getProject);
router.get('/all/:userid', verifyToken, projectController.fetchAllProjects);
router.put('/:id', verifyToken, projectController.updateProjectData);
router.delete('/:id', verifyToken, projectController.deleteProject);

module.exports = router;