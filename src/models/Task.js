const { Model } = require('objection');
const Project = require('./Project');

class Task extends Model {
    static get tableName() {
        return 'tasks';
    }

    static get idColumn() {
        return 'id'; // Primary key column
    }

    // Hook to automatically update project's done field when tasks are marked as done
    // async $beforeUpdate(opt, context) {
    //     // Call the base class's $beforeUpdate hook first
    //     await super.$beforeUpdate(opt, context);

    //     // Construct the query to find tasks that are not done in the same project
    //     let query = Task.query().where({ project_id: this.project_id });

    //     if (this.id !== undefined) {
    //         // Exclude the current task from the check
    //         query = query.whereNot(builder => builder.where('id', this.id));
    //     }

    //     query = query.where(builder => builder.where('done', false));

    //     const allTasksDone = await query.count().first();

    //     if (allTasksDone.count === '0') {
    //         // Update the project's done field to true
    //         await Project.updateDoneStatus(this.project_id, true);
    //     }
    // }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['title', 'description', 'project_id'],
            properties: {
                id: { type: 'integer' },
                title: { type: 'string' },
                description: { type: 'string' },
                done: { type: 'boolean', default: false },
                project_id: { type: 'integer' },
            },
        };
    }

    static get relationMappings() {
        return {
            project: {
                relation: Model.BelongsToOneRelation,
                modelClass: Project,
                join: {
                    from: 'tasks.project_id',
                    to: 'projects.id'
                }
            }
        };
    }
}

module.exports = Task;