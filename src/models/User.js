const { Model } = require('objection');
const Project = require('./Project');
const bcrypt = require('bcrypt');

class User extends Model {
    static get tableName() {
        return 'users';
    }

    async $beforeInsert(context) {
        await super.$beforeInsert(context);
        this.password = await bcrypt.hash(this.password, 10);
    }

    async verifyPassword(password) {
        return bcrypt.compare(password, this.password);
    }

    static get idColumn() {
        return 'id'; // Explicitly define the primary key column name
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['username', 'email', 'password'],
            properties: {
                id: { type: 'integer' },
                username: { type: 'string', minLength: 1, maxLength: 255 },
                email: { type: 'string', format: 'email', maxLength: 255 },
                password: { type: 'string', minLength: 6 },
                name: { type: 'string', maxLength: 255 },
            },
        };
    }

    static get relationMappings() {
        return {
            projects: {
                relation: Model.HasManyRelation,
                modelClass: Project,
                join: {
                    from: 'users.id',
                    to: 'projects.user_id'
                }
            }
        };
    }
}

module.exports = User;