const { Model } = require('objection');
const Task = require('./Task');
const User = require('./User');

class Project extends Model {
    static get tableName() {
        return 'projects';
    }

    static get idColumn() {
        return 'id'; // Primary key column
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['title', 'description', 'user_id'],
            properties: {
                id: { type: 'integer' },
                title: { type: 'string', minLength: 1, maxLength: 255 },
                description: { type: 'string' },
                done: { type: 'boolean', default: false },
                user_id: { type: 'integer' },
            },
        };
    }

    static get relationMappings() {
        return {
            user: {
                relation: Model.BelongsToOneRelation,
                modelClass: User,
                join: {
                    from: 'projects.user_id',
                    to: 'users.id'
                }
            },
            tasks: {
                relation: Model.HasManyRelation,
                modelClass: Task,
                join: {
                    from: 'projects.id',
                    to: 'tasks.project_id'
                }
            }
        };
    }
}

module.exports = Project;