const projectService = require('../services/proejctServices');

exports.getProject = async (request, response) => {
    try {
        const { id } = request.params;
        const project = await projectService.getProject(id);
        if (!project) {
            return response.status(404).json({ success: false, message: 'Project not found' });
        }
        response.json({ success: true, message: 'Project found.', data: project });
    } catch (error) {
        console.error(error);
        response.status(500).json({ success: false, message: error.message });
    }
}

exports.createProject = async (request, response) => {
    try {
        const newProject = await projectService.createProject(request.body);
        response.json({ success: true, message: 'New project created.', data: newProject });
    } catch (error) {
        console.error(error);
        response.status(500).json({ success: false, message: error.message });
    }
};

exports.updateProjectData = async (request, response) => {
    try {
        const projectId = request.params.id; // Extract project ID from URL
        const project = await projectService.getProject(projectId);
        if (!project) {
            return response.status(404).json({ success: false, message: 'Project not found' });
        }
        if (project.user_id !== request.user.id) {
            return response.status(403).json({ success: false, message: 'Access denied: Project does not belong to the user' });
        }
        const updatedProject = await projectService.updateProject(+projectId, request.body);
        response.json({ sucess: true, message: 'Project updated successfully.', data: updatedProject });
    } catch (error) {
        console.error(error);
        response.status(500).json({ success: false, message: error.message });
    }
}

exports.deleteProject = async (request, response) => {
    try {
        const id = request.params.id;
        const project = await projectService.getProject(id);
        if (!project) {
            return response.status(404).json({ success: false, message: 'Project not found' });
        }
        if (project.user_id !== request.user.id) {
            return response.status(403).json({ success: false, message: 'Access denied: Project does not belong to the user' });
        }
        const deletedProject = await projectService.deleteProjectById(+id)
        if (!deletedProject) {
            return response.status(404).json({ success: false, message: 'Project not found' });
        }
        response.json({ success: true, message: 'Project deleted successfully' });
    } catch (error) {
        response.status(500).json({ success: false, message: error.message });
    }
}

exports.fetchAllProjects = async (req, res) => {
    try {
        const userid = req.params.userid;
        if (userid != req.user.id) {
            return res.status(403).json({ success: false, message: 'Access denied: Project does not belong to the user' });
        }
        const allProjects = await projectService.getAllProjects(+userid);
        // console.log(`all projects of user id ${userid}:`, allProjects)
        // console.log(`type of all projects:`, Array.isArray(allProjects))
        // if (!allProjects) {
        //     return res.json({ success: true, message: 'User does not have any projects.' });
        // }
        if (allProjects && allProjects.length === 0) {
            return res.json({ success: true, message: 'User does not have any projects.' })
        }
        res.json({ success: true, message: 'Projects successfully found.', data: allProjects });
    } catch (error) {
        res.status(500).json({ success: false, message: error.message });
    }
};