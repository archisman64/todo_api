const userService = require('../services/userServices');
const jwt = require('jsonwebtoken');

exports.getUserProfile = async (request, response) => {
    try {
        const { username } = request.params;
        const user = await userService.getUser(username);
        if (!user) {
            return response.status(404).json({ success: false, message: 'User not found' });
        }
        response.json({ success: true, message: 'Profile found.', data: user });
    } catch (error) {
        console.error(error);
        response.status(500).json({ success: false, message: error.message });
    }
};

exports.createUser = async (request, response) => {
    try {
        const newUser = await userService.createUser(request.body);
        response.json({ success: true, message: 'New user created.', data: newUser });
    } catch (error) {
        console.error(error);
        response.status(500).json({ success: false, message: error.message });
    }
};

// exports.updateUserProfile = async (request, response) => {
//     try {

//     } catch (error) {
//         console.error(error);
//         response.status(500).json({ success: false, message: error.message });
//     }
// }

exports.signUpUser = async (req, res) => {
    try {
        const { username, email, password, name } = req.body;

        // Check if the username or email is already in use
        const existingUser = await userService.getUser(username);
        const existingEmail = await userService.getUserByEmail(email);

        if (existingUser) {
            return res.status(400).json({ success: false, message: 'Username is already taken.' });
        }

        if (existingEmail) {
            return res.status(400).json({ success: false, message: 'Email is already in use.' });
        }

        // Create a new user
        const newUser = await userService.createUser({
            username,
            email,
            password,
            name,
        });

        return res.status(201).json({ success: true, message: 'User created successfully.', data: newUser });
    } catch (error) {
        console.error(error);
        res.status(500).json({ success: false, message: 'An error occurred while signing up.' });
    }
};

exports.loginUser = async (req, res) => {
    try {
        const { username, password } = req.body;

        // Find the user by username
        const user = await userService.getUser(username);
        if (!user) {
            return res.status(404).json({ success: false, message: 'User not found.' });
        }

        // Verify the provided password
        const isPasswordValid = await user.verifyPassword(password);
        if (!isPasswordValid) {
            return res.status(401).json({ success: false, message: 'Invalid password.' });
        }

        // Generate a JWT token
        const token = jwt.sign({ email: user.email }, process.env.JWT_SECRET, { expiresIn: '1h' });
        return res.status(200).json({ success: true, data: token });
    } catch (error) {
        console.error(error);
        res.status(500).json({ success: false, message: 'An error occurred while logging in.' });
    }
};

exports.updateProfile = async (req, res) => {
    try {
        const { username } = req.params;
        const user = await userService.getUser(username);
        if (req.user.id !== user.id) {
            return res.status(401).json({ success: false, message: 'Unauthorized' });
        }
        const updatedProfile = await userService.updateUser(req.user.id, req.body);
        res.json({ success: true, message: 'Profile updated successfully', data: updatedProfile });
    } catch (error) {
        console.error(error);
        res.status(500).json({ success: false, message: error.message });
    }
}