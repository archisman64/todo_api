const taskService = require('../services/taskServices');

exports.createTask = async (request, response) => {
    try {
        const newTask = await taskService.createTask(request.body);
        response.json({ success: true, message: 'Successfully added new task.', data: newTask });
    } catch (error) {
        console.error(error);
        response.status(500).json({ success: false, message: error.message });
    }
};

exports.updateTaskData = async (request, response) => {
    try {
        const tasktId = request.params.id; // Extract project ID from URL
        const updatedTask = await taskService.updateTask(tasktId, request.body);
        if (!updatedTask) {
            return response.status(404).json({ success: false, message: 'Could not find the task.' });
        }
        response.json({ success: true, message: 'Successfully updated task.', data: updatedTask });
    } catch (error) {
        console.error(error);
        response.status(500).json({ success: false, message: error.message });
    }
}

exports.deleteTask = async (request, response) => {
    try {
        const id = request.params.id;
        const deletedTask = await taskService.deleteTaskById(id)
        if (!deletedTask) {
            return response.status(404).json({ success: false, message: 'Task not found' });
        }
        response.json({ success: true, message: 'Task deleted successfully' });
    } catch (error) {
        response.status(500).json({ success: false, message: error.message });
    }
}
