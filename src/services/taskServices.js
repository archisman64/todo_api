const taskDAO = require('../dao/task');

const createTask = (taskIno) => {
    return taskDAO.createTask(taskIno);
}

const updateTask = (taskId, newInfo) => {
    return taskDAO.updateTask(taskId, newInfo);
}

const deleteTaskById = (taskId) => {
    return taskDAO.deleteTask(taskId);
}

const findTaskById = (id) => {
    return taskDAO.findTaskById(id);
}

module.exports = {
    createTask,
    updateTask,
    deleteTaskById,
    findTaskById
}