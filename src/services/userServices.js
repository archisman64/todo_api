const userDAO = require('../dao/user');

const getUser = (userName) => {
    return userDAO.findByUsername(userName);
}

const getUserByEmail = (email) => {
    return userDAO.findByEmail(email);
}

const createUser = (userInfo) => {
    return userDAO.insertUser(userInfo);
}

const updateUser = (userId, modifiedProfile) => {
    return userDAO.updateUser(userId, modifiedProfile);
}

module.exports = {
    getUser,
    getUserByEmail,
    createUser,
    updateUser
};