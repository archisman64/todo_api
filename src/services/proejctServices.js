const projectDAO = require('../dao/project');

const createProject = (projectInfo) => {
    return projectDAO.insertProject(projectInfo);
}

const getProject = (projectId) => {
    // console.log('porject id in service:', projectId)
    return projectDAO.findProject(projectId);
}

const getAllProjects = (userId) => {
    return projectDAO.findAllProjects(userId);
}

const updateProject = (projectId, newInfo) => {
    return projectDAO.updateProject(projectId, newInfo);
}

const deleteProjectById = (projectId) => {
    return projectDAO.removeProject(projectId);
}

module.exports = {
    createProject,
    getProject,
    updateProject,
    deleteProjectById,
    getAllProjects
}