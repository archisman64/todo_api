/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
exports.seed = async function (knex) {
  // Deletes ALL existing entries
  await knex('projects').del()
  await knex('projects').insert([
    {
      title: 'Project One',
      description: 'Description for Project One',
      user_id: 1, // ID of user1
    },
    {
      title: 'Project Two',
      description: 'Description for Project Two',
      user_id: 1
    },
    {
      title: 'Project Three',
      description: 'Description for Project Three',
      user_id: 2, // ID of user1
    },
  ]);
};
