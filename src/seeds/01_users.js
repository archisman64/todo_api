/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
exports.seed = async function (knex) {
  // Deletes ALL existing entries
  await knex('users').del()
  await knex('users').insert([
    {
      username: 'user1',
      email: 'user1@example.com',
      password: 'password1', // Hashed password
      name: 'User One',
    },
    {
      username: 'user2',
      email: 'user2@example.com',
      password: 'password2', // Hashed password
      name: 'User Two',
    },
  ]);
};
