/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
exports.seed = async function (knex) {
  // Deletes ALL existing entries
  await knex('tasks').del()
  await knex('tasks').insert([
    {
      title: 'Task One',
      description: 'Task 1 for Project One',
      project_id: 1, // ID of Project One
    },
    {
      title: 'Task Two',
      description: 'Task 2 for Project One',
      project_id: 1
    },
    {
      title: 'Task One',
      description: 'Task 1 for Project Two',
      project_id: 2
    },
  ]);
};
